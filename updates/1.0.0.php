<?php namespace Monologophobia\Adverts;

use Schema;
use October\Rain\Database\Updates\Migration;
use Monologophobia\Adverts\Models\Category;

class onePointZeroPointZero extends Migration {

    public function up() {

        Schema::create('mono_adverts_categories', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $category = new Category;
        $category->name = 'Leaderboard (728x90)';
        $category->save();
        $category = new Category;
        $category->name = 'Large Rectangle (336x280)';
        $category->save();

        Schema::create('mono_adverts', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('mono_adverts_categories')->onDelete('cascade');
            $table->boolean('active')->default(true);
            $table->string('link')->nullable();
            $table->string('image')->nullable();
            $table->text('html')->nullable();
            $table->integer('impressions')->default(0);
            $table->integer('clicks')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_adverts');
        Schema::dropIfExists('mono_adverts_categories');
    }

}
