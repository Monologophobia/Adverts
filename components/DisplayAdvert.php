<?php namespace Monologophobia\Adverts\Components;

use Redirect;

use Cms\Classes\Page;

use Monologophobia\Adverts\Models\Category;
use Monologophobia\Adverts\Models\Advert;

class DisplayAdvert extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Display Advert',
            'description' => 'Displays a random advert by category'
        ];
    }

    public function defineProperties() {
        return [
            'category' => [
                'title'   => 'Category',
                'type'    => 'dropdown',
                'default' => 1
            ]
        ];
    }

    public function getCategoryOptions() {
        return Category::lists('name', 'id');
    }

    public function onRun() {
        $category = intval($this->property('category'));
        if ($category = Category::find($category)) {
            $advert = $category->adverts()->active()->inRandomOrder()->first();
        }
        else {
            $advert = Advert::active()->inRandomOrder()->first();
        }
        if ($advert) $advert->increment('impressions');
        $this->page['advert'] = $advert;
    }

    public function onClick() {
        $id = intval(post('advert_id'));
        $advert = Advert::find($id);
        $advert->increment('clicks');
        if ($advert->link) return Redirect::to($advert->link);
    }

}
