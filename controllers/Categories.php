<?php namespace Monologophobia\Adverts\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Adverts\Models\Category;

class Categories extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Adverts', 'adverts', 'categories');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            try {
                foreach ($checkedIds as $id) {
                    $category = Category::findOrFail($id);
                    $category->delete();
                }
                Flash::success('Deleted');
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }
            return $this->listRefresh();
        }
    }

}
