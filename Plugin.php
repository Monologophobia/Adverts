<?php namespace Monologophobia\Adverts;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase {

    // Returns details about the plugin
    public function pluginDetails() {
        return [
            'name'        => 'Adverts',
            'description' => 'Display Adverts in locations',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-newspaper-o'
        ];
    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'adverts' => [
                'label'       => 'Adverts',
                'url'         => Backend::url('monologophobia/adverts/adverts'),
                'icon'        => 'icon-newspaper-o',
                'order'       => 610,

                'sideMenu' => [
                    'adverts' => [
                        'label'       => 'Adverts',
                        'url'         => Backend::url('monologophobia/adverts/adverts'),
                        'icon'        => 'icon-newspaper-o'
                    ],
                    'categories' => [
                        'label'       => 'Categories',
                        'url'         => Backend::url('monologophobia/adverts/categories'),
                        'icon'        => 'icon-list'
                    ]
                ]
            ]
        ];
    }

    public function registerComponents() {
        return [
           '\Monologophobia\Adverts\Components\DisplayAdvert' => 'displayadvert'
        ];
    }

}