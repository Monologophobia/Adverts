<?php namespace Monologophobia\Adverts\Models;

use \October\Rain\Database\Model;

class Advert extends Model {

    // The table to use
    public $table = 'mono_adverts';
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    protected $nullable = ['link', 'image', 'html'];

    public $belongsTo = [
        'category' => ['Monologophobia\Adverts\Models\Category', 'key' => 'category_id']
    ];

    public function scopeActive($query) {
        return $query->where('active', true);
    }

    public function isImage() {
        if ($this->link && $this->image) return true;
        if ($this->html) return false;
        return false;
    }

}