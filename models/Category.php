<?php namespace Monologophobia\Adverts\Models;

use \October\Rain\Database\Model;

class Category extends Model {

    // The table to use
    public $table = 'mono_adverts_categories';
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    protected $fillable = ['name'];

    public $hasMany = [
        'adverts' => ['Monologophobia\Adverts\Models\Advert', 'key' => 'category_id', 'scope' => 'active', 'delete' => true]
    ];

}